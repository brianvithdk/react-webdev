const drink = {
  color: 'brown',
  carbonated: true,
  sugar: 40,
};

// Type alias
type Drink = [string, boolean, number];

const pepsi: Drink = ['brown', true, 40];
const sprite: Drink = ['clear', true, 45];
const tea: Drink = ['brown', true, 0];

// Why tuples are not often used
const carSpecs: [number, number] = [400, 3354]; // harder to understand at a glance
const carStats = {
  horsepower: 400,
  weight: 3354,
};

export {};
