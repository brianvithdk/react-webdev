//const carMakers: string[] = [];
const carMakers = ['ford', 'toyota', 'chevy'];
const dates = [new Date(), new Date()];

const carsByMake = [['f1250'], ['corolla'], ['camaro']];
//const carsByMake: string[][] = [['f1250'], ['corolla'], ['camaro']]; // array with array of strings

// Help with inference when extracing values
const car = carMakers[0];
const myCar = carMakers.pop();

// Prevent incompatible values
carMakers.push(100);

// Help with 'map'
carMakers.map((car: string): string => {
  return car.toUpperCase();
});

// Flexible types
const importantDates: (Date | string)[] = [new Date()];
importantDates.push('2030-10-10');
importantDates.push(new Date());
importantDates.push(100);
