class Vehicle {
  //color: string = 'red';

  constructor(public color: string) {}

  //public is standard if nothing mentioned
  protected honk(): void {
    console.log('dyt dyt');
  }
}

//const vehicle = new Vehicle();
const vehicle = new Vehicle('red');
console.log(vehicle.color);

// inherit Vehicle
class Car extends Vehicle {
  constructor(public wheels: number, color: string) {
    super(color);
  }

  // override method
  private drive(): void {
    console.log('vroom');
  }

  startDrivingProcess(): void {
    this.drive();
    this.honk();
  }
}

const car = new Car(4, 'red');
car.startDrivingProcess();
