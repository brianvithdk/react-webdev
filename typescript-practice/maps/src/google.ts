let googleMap: google.maps.Map;

export function initMap(): void {
  googleMap = new google.maps.Map(
    document.getElementById('map') as HTMLElement,
    {
      center: { lat: 0, lng: 0 },
      zoom: 1,
    }
  );
}

declare global {
  interface Window {
    initMap: () => void;
  }
}
window.initMap = initMap;
