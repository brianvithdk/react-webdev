import {Child} from './Child';

const Parent = () => {
  return <Child color='red' onClick={() => console.log('clicked')}>
    <div>stuff</div>
  </Child>
};

export default Parent;