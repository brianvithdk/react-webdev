interface ChildProps {
  color: string;
  onClick: () => void;
  children: React.ReactNode;
}

export const Child = ({color, onClick}: ChildProps) => {
  return <div>
    {color}
    <button onClick={onClick}>Click me</button>
  </div>;
};

// ***  ReactFC er nu discouraged i community.
export const ChildAsFC: React.FC<ChildProps> = ({color, onClick}) => {
  return <div>
    {color}
    <button onClick={onClick}>Click me</button>
    </div>;
;}
// don't use this

