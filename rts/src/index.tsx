import ReactDOM from "react-dom/client";
import UserSearchRefs from "./refs/UserSearchRefs";
// const users = [
//   {name: 'Sarah', age: 20},
//   {name: 'Alex', age: 20},
//   {name: 'Michael', age: 20},
// ];


const App = () => {
  return (
  <div>
    <UserSearchRefs/>
  </div>
  );
};

ReactDOM.createRoot(document.getElementById('root')!).render(<App/>)